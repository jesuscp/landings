$(function() {

	var tl = new TimelineMax();

	$('#fullpage').fullpage({
		menu: '#links',
		css3: false,
		sectionsColor : ['#f2f2f2', '#f2f2f2','#f2f2f2','#f2f2f2'],
		anchors:['home', 'services','pricing','contacUs'],
		responsiveWidth: 1441,
		onLeave: function(index, nextIndex, direction){
			var leavingSection = $(this);

			if(index == 1 && direction =='down'){
				$('header').addClass('sombra');
				TweenMax.to($('#links'), .5, {width: '50%'});
				TweenMax.to($('.contacUsLink'), .5, {bottom:0});
			}

			if(index == 2 && direction == 'up'){
				$('header').removeClass('sombra');
				tl.to($('.contacUsLink'), .2, {bottom: '-5rem'})
				.to($('#links'), .5, {width: '40%'});
			}

			if(index == 3 && direction == 'down'){
				//tl.to($('button'),.5,{scale: 1.5, yoyo:true, repeat: -1});
			}
		},
		afterLoad: function(anchorLink, index){
			var loadedSection = $(this);

			if(index == 2){
				tl.staggerTo($('.serviceComponent'),1,{top: 0, opacity: 1},0.4);
				TweenMax.to($('.servicesTittle'), .5, {opacity: 1});
			}

			if(index == 3){
				tl.to($('.pcLeft'),.5,{top: 0, left: 0, opacity: 1},-.7);
				tl.to($('.pcMiddle'),.5,{top: 0, opacity: 1},-.7);
				tl.to($('.pcRight'),.5,{top: 0, left: 0, opacity: 1},-.7);
				TweenMax.to($('.planesTittle'), .5, {opacity: 1});
			}

			var $body = document.body;
			var gestos = new Hammer($body);
			gestos.on('swipeleft', function(){
				tl.to($('.menuResponsiveMainContainer'),.3,{left: 0});
			});

			gestos.on('swiperight', function(){
				tl.to($('.menuResponsiveMainContainer'),.3,{left: '100%'});
			});
		},
		afterResize: function() {
			var pluginContainer = $(this);
			if ($(this).width() >= 769) {
				$.fn.fullpage.setFitToSection(true);
				$.fn.fullpage.setAutoScrolling(true);
			} else {
				$.fn.fullpage.setFitToSection(false);
				$.fn.fullpage.setAutoScrolling(false);
			}
		},
		afterRender: function() {
			var pluginContainer = $(this);
			if ($(this).width() >= 769) {
				$.fn.fullpage.setFitToSection(true);
				$.fn.fullpage.setAutoScrolling(true);
			} else {
				$.fn.fullpage.setFitToSection(false);
				$.fn.fullpage.setAutoScrolling(false);
			}
		}
	});

	$('.btnResponsive').on('click',function(){
		tl.to($('.menuResponsiveMainContainer'),.5,{left: 0});
	})

	$('.btnClose').on('click',function(){
		tl.to($('.menuResponsiveMainContainer'),.5,{left: '100%'});
	})

	$('.menuResponsiveContainer').on('click','a',function(){
		tl.to($('.menuResponsiveMainContainer'),.5,{left: '100%'});
	});

});